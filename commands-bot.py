                self.archiveTemplateName = u"틀:완료된 토론 자동 보존"
                self.excludeList = (
                       self.archiveTemplateName,
                       )
 
                self.errorCategory = u"분류:잘못된 자동 보존 설정이 있는 문서"
                self.errorText = u"== 보존을 처리하지 못했습니다 ==\n"
                self.errorText += u"The bot-run at 봇이 ~~~~~ 에 토론을 보존하려 했으나, 성공적이지 못했습니다. 틀  " + self.archiveTemplateName + u" 이 부정확한 변수를 포함했기 때문입니다.  %s"
                self.errorText += u"[[" + self.archiveTemplateName + u"|설명 문서]] 를 보고 문제를 고쳐 주세요. 감사합니다.--~~~~"
                self.errorText += u"\n\n[[" + self.errorCategory + u"]]<!--문제가 해결되었다면 이 줄을 지우세요. -->"
                self.errorTextSummary = u"문제 보고"
                # template parameters / 틀 변수
                self.optionsRegEx = u"\{\{\ *(?:틀\:)?\ *완료된\ 토론\ 자동 \ 보존\(?P<옵션>.*?)\}\}"
                self.paramAge = u'날짜'
                self.paramArchive = u'보존'
                self.paramLevel = u'단위'
                self.paramTimeComparator = u'시간 비교'
                self.paramTimeComparatorCleared = u'완료된 토론'
                # edit summaries / 편집 요약
                self.archiveSumTargetS = u"1개의 문단을 [[%s]] 에서 가져옴"
                self.archiveSumTargetP = u"%d 개의 문단을 [[%s]] 에서 가져옴"
                self.archiveSumOriginS = u"1개의 문단"
                self.archiveSumOriginP = u"%d개의 문단"
                self.archiveSumOriginMulti  = u"%d 을 [[%s]]"
                self.archiveSumLastEdit= u" - 이전 편집: [[User:%s|%s]], %s"
                self.archiveSumArchive = u"보존 %s: %s"
                self.sectResolvedRegEx = u"(?:완료된 토론)"
                self.sectResolved1P    = u":<small>이 문단은 다음 사용자의 요청에 의해 보존되었습니다: \\1</small>"
                self.sectResolved2P    = u":<small>이 문단은 다음 사용자의 요청에 의해 보존되었습니다 \\1 \\2</small>"
